# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 17:53:11 2020

@author: Dimitri Nseng

Version du mardi 03 novembre 2020
"""
from Formule import *
from FormuleBinaire import *
from Et import *
from Ou import *
from Implique import *
from EquivalentA import *
from OuEx import *
from FormuleUnaire import *
from Non import *
from FormuleArite0 import *
from Variable import *
from Interpretation import *
from BaseConnaissances import *

import ParseFormule

#------------- Exercice 1 --------------------
def sep() :
    print("\n====================\n")

def constructeurphi():
    # Représentation de la formule phi en utilisant les constructeurs de formules
    a  = Variable("a")
    b = Variable("b")
    phi = Implique(OuEx(a,b), Ou(Non(a), Non(b)))
    print("phi =", phi.str_prefixe())
    
def parseurphi():
    # Représentation de la formule phi en utilisant le parseur
    ch1 = "=>(<+>(a,b), Ou(Non(a), Non(b)))"
    phi1 = ParseFormule.exec(ch1)
    #print("ch1", ch1)
    print("phi1 =", phi1.str_prefixe())
    
#------------- Exercice 2 --------------------
    
def nb_interp(phi) :
    # Nombre d'interpretations d'une formule phi
    nb = 0
    if type(phi) == str :
        phi = ParseFormule.exec(phi)
        for interp in Interpretation.iterateur_interpretations(list(phi.get_variables())) :
            if interp.satisfait(phi):
                nb += 1
    return nb
                
#------------- Exercice 3 --------------------

def satisfiable(phi) :
    # Tester qu’une formule phi est satisfiable (en vous arrêtant dès
    # que vous avez trouvé une interprétation qui convienne)
    
    if type(phi) == str :
        phi = ParseFormule.exec(phi)
        # On va commencer par récupérer toutes les variables
        # comme ça, s'il n'y a pas de variables ça lève une exception
        var = phi.get_variables()
        for interp in Interpretation.iterateur_interpretations(list(var)) :
            if interp.satisfait(phi):
                return True
    return False

#------------- Exercice 4 --------------------

def tautologie(phi) :
    # Tester qu’une formule phi est une tautologie
    if type(phi) == str :
        phi = ParseFormule.exec(phi)
        var = phi.get_variables()
        for interp in Interpretation.iterateur_interpretations(list(var)) :
            if interp.satisfait(Non(phi)): # la clé de voûte est ici!! (relire l'exo!)
                return False
    return True

#------------- Exercice 5 --------------------

def entraine(phi, psi) :
    # Tester si la formule phi => psi est une tautologie
    
    # beh je ne trouve pas de moyen plus efficace, après longue réflexion
    # donc j'écris la formule comme ça!
    
    formule = "=>"+"("+phi+","+psi+")" # je rappelle que c'est une char
    return tautologie(formule)

def equivalentes(phi, psi):
    # Tester si la formule phi <=> psi est une tautologie
    formule = "<=>"+"("+phi+","+psi+")" 
    return tautologie(formule)
    
    
#------------- Exercice 6 --------------------
""" la classe BaseConnaissances est dans le fichier script BaseConnaissances.py"""

def conjonction(base):
    # base : l'ensemble des formules de la base de connaissances
    # sortie : le produit des formules par l'operateur ET()
    top = "TOP"
    for form in BaseConnaissances(base).get_formules():
        top = "et"+"("+form+","+top+")"
        #top = ParseFormule.exec(top)
        #print("\n",top)
    #top = ParseFormule.exec(top)
    return top

def entrainebase(base, phi):
    # Testez si une base de connaissances entraine une formule
    # i.e le produit de ET() implique phi est une tautologie
    prodformule = conjonction(base)
    return entraine(prodformule, phi)

#------------- Exercice 7 --------------------

#------------- Exercice 8 --------------------

#------------- Exercice 9 --------------------

#------------- Exercice 10 --------------------

#------------- Exercice 11 --------------------

#------------- Exercice 12 --------------------

#------------- Tests --------------------

def test_exercice1():
    sep()
    print("==== Exercice 1 ====")
    sep()
    print("---- Constructeur ---")
    constructeurphi()
    print("\n---- Parseur ---")
    parseurphi()

def test_exercice2(phi):
    sep()
    print("==== Exercice 2 ====")
    sep()
    #phi = ParseFormule.exec(phi)
    #print("phi = ", phi.str_prefixe())
    print("Le nombre d'interpretations de phi est :",nb_interp(phi))

def test_exercice3(phi):
    sep()
    print("==== Exercice 3 ====")
    sep()
    #phi = ParseFormule.exec(phi)
    #print("phi = ", phi.str_prefixe())
    print("La formule phi est-telle satisfiable ?:", satisfiable(phi))  

def test_exercice4(phi):
    # Tester qu’une formule est une tautologie
    sep()
    print("==== Exercice 4 ====")
    sep()
    #phi = ParseFormule.exec(phi)
    #print("phi = ", phi.str_prefixe())
    print("La formule phi est-t-elle une tautologie ?:", tautologie(phi))  
    
def test_exercice5(phi, psi):
    # Tester si la formule phi => psi est une tautologie
    sep()
    print("==== Exercice 5 ====")
    sep()
    
    print("La formule phi entraine psi ?:", entraine(phi, psi)) 
    print("\nLes formules phi et psi sont equivalentes ?:", equivalentes(phi, psi))
    
def test_exercice6(base, phi):
    # Testez si une base de connaissances entraine une formule
    sep()
    print("==== Exercice 6 ====")
    sep() 
    print("La base entraine-t-elle la formule phi ?:",  entrainebase(base, phi))
    
# Les formules utilisées
phi ="et(a,b)"
phi1 = "ou(a,b)"
phi2 = "=>(<+>(a,b), Ou(Non(a), Non(b)))"
phi3 = "et(a, non(ou(b, c)))"
phi4 = "=>(Non(Non(a)), a)"
phi5 = "<=>(et(a,b), et(b,a))"
phi6 = "=>(et(=>(p,q),p),q)"
phi7 = "et(=>(p,q),p)"
phi8 = "et(p, ou(q,r))"
psi1 = "q" 
psi2 = "ou(et(p,q),et(p,r))"

base = [phi, phi1]

test_exercice1()

test_exercice2(phi4)

test_exercice3(phi4)

test_exercice4(phi3)

test_exercice5(phi8, psi2)

test_exercice6(base, psi1)

