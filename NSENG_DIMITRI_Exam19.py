# coding=utf-8

# Rendu de l'examen de TP d'APA
# auteur : Nseng Dimitri
# date : **

from Formule import *
from FormuleBinaire import *
from Et import *
from Ou import *
from Implique import *
from EquivalentA import *
from OuEx import *
from FormuleUnaire import *
from Non import *
from FormuleArite0 import *
from Variable import *
from Interpretation import *

import ParseFormule

def sep() :
    print("\n====================\n")

## Question 1
# phi : Formule
# sortie : int
def nombre_et(phi) :
    
    if isinstance(phi, FormuleArite0):
        return 0
    elif isinstance(phi, FormuleUnaire) :
        return nombre_et(phi.get_oper())
    elif isinstance(phi,FormuleBinaire):
        if isinstance(phi,Et):
            return 1 + nombre_et(phi.get_oper1()) + nombre_et(phi.get_oper2())
        else:
            return nombre_et(phi.get_oper1()) + nombre_et(phi.get_oper2())
    else:
        return -1

def test_question1() :
    print("** à faire... **")
    #phi = ParseFormule.exec("et(a, non(ou(b, c)))")
    phi = ParseFormule.exec("Et(a, Non(Ou(b,Et(c, Non(d)))))")
    #phi = ParseFormule.exec("Non(Ou(b,Et(c, Non(d))))")
    print("Nombre de ET :",nombre_et(phi))

## Question 2

# C : classe
# phi : Formule
# sortie : int
def nombre_de(C, phi) :
    
    if isinstance(phi, Variable):
        if C == Variable : 
            return len(list(phi.get_variables()))
    elif isinstance(phi, Formule):
        if C == Formule :
            return 1 + nombre_de(C, phi.get_racine())
            
    if C == FormuleArite0:
        if isinstance(phi, FormuleArite0):
            return 1
        else : 
            return 0
           
    if isinstance(phi, FormuleUnaire) :
        if C == Non:
            if isinstance(phi, Non):
                return 1 + nombre_de(C,phi.get_oper())
            else : 
                return nombre_de(C,phi.get_oper())
                
        if C == FormuleUnaire :  
            return 1 + nombre_de(C,phi.get_oper())
        else : 
            return nombre_de(C,phi.get_oper())
        
    elif isinstance(phi,FormuleBinaire):
        if C == Et :
            if isinstance(phi, Et):
                return 1 + nombre_de(C, phi.get_oper1()) + nombre_de(C, phi.get_oper2())
            else:
                return  nombre_de(C, phi.get_oper1()) + nombre_de(C, phi.get_oper2())
        
        if C == Ou :
            if isinstance(phi, Ou):
                return 1 + nombre_de(C, phi.get_oper1()) + nombre_de(C, phi.get_oper2())
            else:
                return  nombre_de(C, phi.get_oper1()) + nombre_de(C, phi.get_oper2())
        
        if C == OuEx :
            if isinstance(phi, OuEx):
                return 1 + nombre_de(C, phi.get_oper1()) + nombre_de(C, phi.get_oper2())
            else:
                return  nombre_de(C, phi.get_oper1()) + nombre_de(C, phi.get_oper2())
        if C == Implique :
            if isinstance(phi, Implique):
                return 1 + nombre_de(C, phi.get_oper1()) + nombre_de(C, phi.get_oper2())
            else:
                return  nombre_de(C, phi.get_oper1()) + nombre_de(C, phi.get_oper2())
        
        if C == EquivalentA :
            if isinstance(phi, EquivalentA):
                return 1 + nombre_de(C, phi.get_oper1()) + nombre_de(C, phi.get_oper2())
            else:
                return  nombre_de(C, phi.get_oper1()) + nombre_de(C, phi.get_oper2())
            
        if C == FormuleBinaire :
            return 1 + nombre_de(C, phi.get_oper1()) + nombre_de(C, phi.get_oper2())
        else:
            return nombre_de(C, phi.get_oper1()) + nombre_de(C, phi.get_oper2())
        
    else:
        return 0

            
            
def test_question2() :
    print("** à faire... **")
    phi = ParseFormule.exec("Et(Non(a), Non(Ou(Non(b),Et(c, Non(d)))))")
    #phi = ParseFormule.exec("Non(Non(Ou(b,Et(c, Non(d)))))")
    print("Nombre d'occurences de variables dans phi :", nombre_de(Variable, phi))
    print("Nombre de NON dans phi :", nombre_de(Non, phi))
    print("Nombre de ET dans phi :", nombre_de(Et, phi))
    print("Nombre de OU dans phi :", nombre_de(Ou, phi))
    print("Nombre de OuEx dans phi :", nombre_de(OuEx, phi))
    print("Nombre de IMPLIQUE dans phi :", nombre_de(Implique, phi))
    print("Nombre de EquivalentA dans phi :", nombre_de(EquivalentA, phi))
    print("Nombre de FormuleArite0 dans phi :", nombre_de(FormuleArite0, phi))
    print("Nombre de TOP dans phi :", nombre_de(top, phi))
    print("Nombre de Formule dans phi :", nombre_de(Formule, phi))
    print("Nombre de FormuleUnaire dans phi :", nombre_de(FormuleUnaire, phi))
    print("Nombre de FormuleBinaire dans phi :", nombre_de(FormuleBinaire, phi))
    
## Exercice 3

# phi : Formule
# sortie : Formule
def mise_sous_FNN(phi) :
    return None

def test_question3() :
    print("** à faire... **")

## Exécution des tests
def tests () :
    test_question1()
    sep()
    test_question2()
    sep()
    test_question3()

tests()

"""
print()
#phi = ParseFormule.exec("Et(a, Non(Ou(b,Et(c, Non(d)))))")
phi = ParseFormule.exec("Non(Ou(b,Et(c, Non(d))))")
print(isinstance(phi, Et))
print(isinstance(phi, Ou))
print(isinstance(phi, FormuleBinaire))
print(isinstance(phi, FormuleUnaire))
print(isinstance(phi, Formule))
"""
